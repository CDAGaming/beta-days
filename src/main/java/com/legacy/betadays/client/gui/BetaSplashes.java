package com.legacy.betadays.client.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.legacy.betadays.BetaDays;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.ReloadListener;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BetaSplashes extends ReloadListener<List<String>>
{
	private static final ResourceLocation SPLASHES_LOCATION = BetaDays.locate("texts/splashes.txt");
	private static final Random RANDOM = new Random();
	private final List<String> possibleSplashes = Lists.newArrayList();

	public BetaSplashes()
	{
	}

	@SuppressWarnings("unchecked")
	public List<String> prepare(IResourceManager resourceManagerIn, IProfiler profilerIn)
	{
		try (IResource iresource = Minecraft.getInstance().getResourceManager().getResource(SPLASHES_LOCATION); BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(iresource.getInputStream(), StandardCharsets.UTF_8));)
		{
			Object object = bufferedreader.lines().map(String::trim).filter((p_215277_0_) ->
			{
				return p_215277_0_.hashCode() != 125780783;
			}).collect(Collectors.toList());
			return (List<String>) object;
		}
		catch (IOException var36)
		{
			return Collections.emptyList();
		}
	}

	protected void apply(List<String> splashList, IResourceManager resourceManagerIn, IProfiler profilerIn)
	{
		this.possibleSplashes.clear();
		this.possibleSplashes.addAll(splashList);
	}

	@Nullable
	public String getSplashText()
	{
		if (this.possibleSplashes.isEmpty())
		{
			return "How did you manage that one?";
		}
		else
		{
			return this.possibleSplashes.get(RANDOM.nextInt(this.possibleSplashes.size()));
		}
	}
}