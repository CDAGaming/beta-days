package com.legacy.betadays;

import com.legacy.betadays.client.BetaClientEvent;
import com.legacy.betadays.client.BetaOverlayEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(BetaDays.MODID)
public class BetaDays
{
	public static final String NAME = "Beta Days";
	public static final String MODID = "beta_days";

	public static void registerEvent(Object obj)
	{
		MinecraftForge.EVENT_BUS.register(obj);
	}

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static String find(String key)
	{
		return new String(MODID + ":" + key);
	}

	public BetaDays()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, BetaDaysConfig.CLIENT_SPEC);
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, BetaDaysConfig.SERVER_SPEC);

		DistExecutor.runWhenOn(Dist.CLIENT, () -> () ->
		{
			FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientInit);
		});

		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonInit);
	}

	private void commonInit(FMLCommonSetupEvent event)
	{
		BetaDays.registerEvent(new BetaPlayerEvents());
	}

	public void clientInit(FMLClientSetupEvent event)
	{
		BetaDays.registerEvent(new BetaClientEvent());
		BetaDays.registerEvent(new BetaOverlayEvent());
	}
}