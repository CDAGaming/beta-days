package com.legacy.betadays;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

@Mod.EventBusSubscriber(modid = BetaDays.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BetaDaysConfig
{
	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	public static boolean disableCombatCooldown;
	public static boolean hungerDisabled;
	public static boolean disableSprinting;
	public static boolean originalBow;
	public static boolean disableExperienceDrop;

	public static boolean oldHud;
	public static boolean disableNetherFog;
	public static boolean customDimensionMessages;
	public static boolean enableClassicMenu;

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	private static String translate(String key)
	{
		return new String(BetaDays.MODID + ".config." + key + ".name");
	}

	@SubscribeEvent
	public static void onLoadConfig(final ModConfig.ModConfigEvent event)
	{
		ModConfig config = event.getConfig();

		if (config.getSpec() == CLIENT_SPEC)
		{
			ConfigBakery.bakeClient(config);
		}
		else if (config.getSpec() == SERVER_SPEC)
		{
			ConfigBakery.bakeServer(config);
		}
	}

	private static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> oldHud;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableNetherFog;
		public final ForgeConfigSpec.ConfigValue<Boolean> customDimensionMessages;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableClassicMenu;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			oldHud = builder.translation(translate("oldHud")).comment("Gives the classic HUD, removing the hunger bar, and adjusting the position of armor bar.").define("oldHud", false);
			disableNetherFog = builder.translation(translate("disableNetherFog")).comment("Removes environmental fog from the Nether Dimension.").define("disableNetherFog", false);
			customDimensionMessages = builder.translation(translate("customDimensionMessages")).comment("Enable custom dimension entry messages. (Vanilla Dimensions)").define("customDimensionMessages", false);
			enableClassicMenu = builder.translation(translate("enableClassicMenu")).comment("Enable a classic menu. (May cause mod compatibility issues when used with other menu mods)").define("enableClassicMenu", false);
			builder.pop();
		}
	}

	private static class ServerConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> disableCombatCooldown;
		public final ForgeConfigSpec.ConfigValue<Boolean> hungerDisabled;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableSprinting;
		public final ForgeConfigSpec.ConfigValue<Boolean> originalBow;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableExperienceDrop;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes.").push("common");
			disableCombatCooldown = builder.translation(translate("disableCombatCooldown")).comment("Disables the 1.9+ combat changes.").define("disableCombatCooldown", false);
			hungerDisabled = builder.translation(translate("hungerDisabled")).comment("Disables hunger, food gives health instead.").define("hungerDisabled", false);
			disableSprinting = builder.translation(translate("disableSprinting")).comment("Disables sprinting.").define("disableSprinting", false);
			originalBow = builder.translation(translate("originalBow")).comment("Allows for instantly shooting bows.").define("originalBow", false);
			disableExperienceDrop = builder.translation(translate("disableExperienceDrop")).comment("Disables mobs dropping experience.").define("disableExperienceDrop", false);

			builder.pop();
		}
	}

	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;

		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
			oldHud = CLIENT.oldHud.get();
			disableNetherFog = CLIENT.disableNetherFog.get();
			customDimensionMessages = CLIENT.customDimensionMessages.get();
			enableClassicMenu = CLIENT.enableClassicMenu.get();
		}

		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
			disableCombatCooldown = SERVER.disableCombatCooldown.get();
			hungerDisabled = SERVER.hungerDisabled.get();
			disableSprinting = SERVER.disableSprinting.get();
			originalBow = SERVER.originalBow.get();
			disableExperienceDrop = SERVER.disableExperienceDrop.get();
		}
	}
}
